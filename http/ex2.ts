

let resp = await fetch("https://example.com", {
    method: "POST",
    headers: {
        "Content-Type": "application/json",
        "X-API-Key": "foobar",
    },
    body: JSON.stringify({
        param: "value",
    }),
});

console.log(resp.headers.get("Content-Type"));
console.log(await resp.text());