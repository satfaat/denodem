import {Encryptor} from './encryptor.js'; // Import Encryptor

async function main() {
    const publicKeyBase64 = "BringMeYourLongKey\n" +
        "BringMeYourLongKey"; // Replace with your actual key
    const data = {message: "This is a secret message.", value: 123};
    const dataString = JSON.stringify(data);

    try {
        const encryptor = new Encryptor(publicKeyBase64);
        const encrypted = await encryptor.encryptData(dataString);
        console.log("Encrypted:", encrypted);

        const decrypted = await encryptor.decryptData(encrypted);
        console.log("Decrypted:", decrypted);
    } catch (error) {
        console.error("Encryption/Decryption error:", error);
    }
}

main();