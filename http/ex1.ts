// deno run --allow-net https://docs.deno.com/examples/scripts/http_requests.ts

let resp = await fetch("https://example.com");
console.log(resp.status); // 200
console.log(resp.headers.get("Content-Type")); // text/html; charset=UTF-8
// console.log(await resp.text()); // html as text


resp = await fetch("https://example.com");
await resp.arrayBuffer();
/** or await resp.json(); */
/** or await resp.blob(); */


resp = await fetch("https://example.com");
for await (const chunk of resp.body!) {
    console.log("chunk", chunk);
}