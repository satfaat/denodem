import {RsaSerializer} from './RsaSerializer.js'; // Import RsaSerializer

export class Encryptor {
    constructor(publicKeyBase64) {
        this.rsaSerializer = new RsaSerializer(publicKeyBase64);
    }

    async encryptData(jsonString) {
        const encrypted = await this.rsaSerializer.encryptWithRSA(jsonString);
        if (!encrypted) {
            throw new Error("Encryption failed");
        }
        return encrypted;
    }

    async decryptData(encryptedData) {
        const decrypted = await this.rsaSerializer.decryptWithRSA(encryptedData);
        if (!decrypted) {
            throw new Error("Decryption failed");
        }
        return decrypted;
    }
}