export class RsaSerializer {
    constructor(publicKeyBase64) {
        this.publicKey = this.loadPublicKey(publicKeyBase64);
    }

    async loadPublicKey(publicKeyBase64) {
        try {
            const keyData = Uint8Array.from(atob(publicKeyBase64), c => c.charCodeAt(0));
            const publicKey = await crypto.subtle.importKey(
                "spki",
                keyData,
                {
                    name: "RSA-OAEP",
                    hash: "SHA-256",
                },
                false,
                ["encrypt",] // Ensure 'encrypt' permission
            );
            console.log("Imported Public Key:", publicKey); // For debugging
            return publicKey;
        } catch (error) {
            console.error("Error creating public key:", error);
            return null;
        }
    }

    async encryptWithRSA(plainText) {
        if (!this.publicKey) {
            console.error("Public key is not loaded.");
            return null;
        }

        try {
            console.log("Data type before conversion:", typeof plainText); // For debugging
            const encodedText = new TextEncoder().encode(plainText); // Convert to Uint8Array
            const encryptedData = await crypto.subtle.encrypt(
                {name: "RSA-OAEP"},
                await this.publicKey,
                encodedText
            );
            return btoa(String.fromCharCode(...new Uint8Array(encryptedData)));
        } catch (error) {
            console.error("Error encrypting data:", error);
            return null;
        }
    }

    async decryptWithRSA(encryptedBase64) {
        if (!this.publicKey) {
            console.error("Public key is not loaded.");
            return null;
        }

        try {
            const encryptedData = new Uint8Array(atob(encryptedBase64).split('')
                .map(char => char.charCodeAt(0)));
            const decryptedData = await crypto.subtle.decrypt(
                {name: "RSA-OAEP"},
                await this.publicKey,
                encryptedData
            );
            const decryptedText = new TextDecoder().decode(decryptedData);
            return JSON.parse(decryptedText); // Parse back to JSON
        } catch (error) {
            console.error("Error decrypting data:", error);
            return null;
        }
    }
}