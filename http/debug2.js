const publicKeyBase64 = "MIIBIjSomeFakeKey\n" +
    "LongAsTtjp4ccr\n" +
    "d5G2Lf";
const jsonData = {message: "This is a secret message.", value: 123};
const jsonString = JSON.stringify(jsonData);

console.log(jsonData)
console.log("jsonString: ", jsonString);

const keyData = Uint8Array.from(atob(publicKeyBase64), c => c.charCodeAt(0));

console.log(keyData);

const publicKey = await crypto.subtle.importKey(
    "spki",
    keyData,
    {
        name: "RSA-OAEP",
        hash: "SHA-256",
    },
    false,
    ["encrypt",] // Ensure 'encrypt' permission
);

console.log("Imported Public Key:", publicKey);

const encodedText = new TextEncoder().encode(jsonString);

console.log("encodedText: ", encodedText);

const encryptedData = await crypto.subtle.encrypt(
    {name: "RSA-OAEP"},
    publicKey,
    encodedText
);
console.log("encryptedData: ", encryptedData);
const tmp = btoa(String.fromCharCode(...new Uint8Array(encryptedData)));
console.log("tmp: ", tmp);