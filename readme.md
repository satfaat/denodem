
## K6

```shell
k6 new

k6 run k6dem/script.js

# 10 virtual user and 30s duration
k6 run --vus 10 --duration 30s script.js
```


## Deno
- https://docs.deno.com/runtime/fundamentals/node/
- https://docs.deno.com/examples/

```shell
deno --version
deno info
deno upgrade

deno run -A main.ts
```

### start a new project 
```shell
deno init
```

### run

```shell
deno run https://docs.deno.com/examples/hello-world.ts
```

### Dependencies
```shell
deno install npm:chalk

# Global installation
deno install --global --allow-net --allow-read jsr:@std/http/file-server
deno install -g https://examples.deno.land/color-logging.ts

deno outdated

deno remove @std/path

deno uninstall @std/dotenv chalk
```