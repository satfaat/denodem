import { executeQuery } from "../configs/db/sqliteEngine.ts";

export async function getAllTodos() {
    return await executeQuery("SELECT * FROM todos");
}

export async function getTodoById(id: string) {
    return await executeQuery("SELECT * FROM todos WHERE id = ?", [id]);
}

export async function createTodo(text: string) {
    await executeQuery("INSERT INTO todos (text) VALUES (?)", [text]);
}

export async function updateTodoById(id: string, text: string) {
    await executeQuery("UPDATE todos SET text = ? WHERE id = ?", [text, id]);
}

export async function deleteTodoById(id: string) {
    await executeQuery("DELETE FROM todos WHERE id = ?", [id]);
}