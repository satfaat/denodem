import { Router } from "https://deno.land/x/oak/mod.ts";
import * as todosHandlers from "./dbHandler.ts";

const router = new Router();

router.get("/todos", async (ctx) => {
    ctx.response.body = await todosHandlers.getAllTodos();
});

router.get("/todos/:id", async (ctx) => {
    const { id } = ctx.params;
    const todo = await todosHandlers.getTodoById(id);
    if (todo.length) {
        ctx.response.body = todo[0];
    } else {
        ctx.response.status = 404;
        ctx.response.body = { message: "Todo not found" };
    }
});

router.post("/todos", async (ctx) => {
    const { text } = await ctx.request.body().value;
    await todosHandlers.createTodo(text);
    ctx.response.status = 201;
    ctx.response.body = { message: "Todo created successfully" };
});

router.put("/todos/:id", async (ctx) => {
    const { id } = ctx.params;
    const { text } = await ctx.request.body().value;
    await todosHandlers.updateTodoById(id, text);
    ctx.response.body = { message: "Todo updated successfully" };
});

router.delete("/todos/:id", async (ctx) => {
    const { id } = ctx.params;
    await todosHandlers.deleteTodoById(id);
    ctx.response.body = { message: "Todo deleted successfully" };
});

export { router };