import { DB } from "https://deno.land/x/sqlite/mod.ts";

export const DB_PATH = "./example.sqlite";

export async function connect(dbPath) {
    const db = new DB(dbPath);
    return db;
}

export async function executeQuery(query: string, params: any[] = []) {
    const db = await connect(DB_PATH);
    const result = await db.query(query, params);
    db.close();
    return result;
}