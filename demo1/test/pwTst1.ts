import {chromium, firefox} from "npm:playwright";

const sleep = (ms) => new Promise((r) => setTimeout(r, ms));

async function main() {
    const browser = await firefox.launch({
        headless: false,
    });
    const page = await browser.newPage();
    await page.goto("http://deno.com");
    await sleep(2000);

    await browser.close();
}

if (import.meta.main) {
    await main();
}