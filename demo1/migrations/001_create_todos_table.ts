import { executeQuery } from "../configs/db/sqliteEngine.ts";

export async function up() {
    await executeQuery(
        `CREATE TABLE IF NOT EXISTS todos (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      text TEXT
    )`
    );
}

export async function down() {
    await executeQuery(`DROP TABLE IF EXISTS todos`);
}