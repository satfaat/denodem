import { Application } from "https://deno.land/x/oak/mod.ts";
import { router } from "./todo/router.ts";
import { connect, DB_PATH } from "./configs/db/sqliteEngine.ts";
import { up as runMigrations } from "./migrations/001_create_todos_table.ts";

const app = new Application();

// Run migrations to initialize the database
await connect(DB_PATH);
await runMigrations();

app.use(router.routes());
app.use(router.allowedMethods());


console.log("Server running on http://localhost:8000");

await app.listen({ port: 8000 });