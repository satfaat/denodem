
Src

- https://deno.com/blog/build-a-graphql-server-with-deno
- https://deno.com/blog/ecommerce-with-perfect-lighthouse-score
- https://github.com/denoland/merch

path:

- localhost:3000/graphql


```shell
deno run --allow-net server.ts
deno run --allow-net --allow-env server.ts
```

## Fresh

```shell
# setup project 
deno run -A -r https://fresh.deno.dev my-fresh-blog

# run server 
deno task start
```