import { executeQuery } from "./configs/db/sqliteEngine.ts";


// Function to retrieve all dinosaurs
const allDinosaurs = async (): Promise<any[]> => {
    const result = await executeQuery("SELECT * FROM dinosaurs");
    return result.rows;
};

// Function to retrieve a dinosaur by name
const oneDinosaur = async (args: { name: string }): Promise<any[]> => {
    const result = await executeQuery(
        "SELECT * FROM dinosaurs WHERE name = ?",
        [args.name]
    );
    return result.rows;
};

// Function to add a new dinosaur
const addDinosaur = async (args: { name: string; description: string }): Promise<any> => {
    const result = await executeQuery(
        "INSERT INTO dinosaurs (name, description) VALUES (?, ?)",
        [args.name, args.description]
    );
    return result.rows[0];
};

// Resolvers for your GraphQL schema
export const resolvers = {
    Query: {
        allDinosaurs,
        oneDinosaur: (_: any, args: any) => oneDinosaur(args),
    },
    Mutation: {
        addDinosaur: (_: any, args: any) => addDinosaur(args),
    },
};
