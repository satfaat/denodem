import { makeExecutableSchema } from "https://deno.land/x/graphql_tools@0.0.2/mod.ts";
import { typeDefs } from "./queries.ts";
import { resolvers } from "./resolvers.ts";


export const schema = makeExecutableSchema({ resolvers, typeDefs });